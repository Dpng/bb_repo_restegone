package com.dpn.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.dpn.testEg.Multiply;

public class testNullCheckMultiply {

	@Test
	public void shouldCheckOnNullObjects(){
		Multiply tester = new Multiply();
		Object myObj = new Object();
		tester.createObject(myObj, false);
		
		assertNull("create null Object", null);
				
		tester.createObject(myObj, true);
		assertNotNull("createObject", myObj);
		
		
		
	}
	
	@Test
	public void shouldBeSameObjects(){
		Multiply tester = new Multiply();
		String originalObj = new String("Original");
		String sameObj = (String)tester.sameObjectTest(originalObj);
		
		assertSame("Equal Objs",originalObj,sameObj);
		
	}
	
	@Test
	public void shouldBeDiffObjects(){
		Multiply tester = new Multiply();
		String diffObj = (String)tester.diffObjectTest();
		
		assertNotSame("Not Equal objs","Dip",diffObj);
	}
}
