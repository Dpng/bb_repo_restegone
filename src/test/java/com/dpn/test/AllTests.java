package com.dpn.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ testMultiply.class, testNullCheckMultiply.class })
public class AllTests {

}
