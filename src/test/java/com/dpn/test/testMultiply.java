package com.dpn.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.dpn.testEg.Multiply;

public class testMultiply {
	

	@Test //The @Test annotation identifies a method as a test method.
	public void multiplicationOfZeroIntegersShouldReturnZero() {

		Multiply tester = new Multiply();

		assertEquals("10 x 0 must be 0", 10, tester.multiply(10, 1));
		assertEquals("0 x 10 must be 0", 0, tester.multiply(0, 10));
		assertEquals("0 x 0 must be 0", 0, tester.multiply(0, 0));

		//fail("Not yet implemented");
	}
	
	@Test
	public void shouldReturnTrueIf10(){
		
		Multiply tester = new Multiply();
		
		boolean conditionTest = tester.booleanCond(10);
		
		assertTrue("should be true if 10 is passed",conditionTest);
		conditionTest = tester.booleanCond(210);
		assertFalse("should be false if 10 is passed",conditionTest);
	}

}
