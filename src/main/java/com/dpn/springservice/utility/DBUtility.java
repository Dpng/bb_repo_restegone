package com.dpn.springservice.utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBUtility {
 private static Connection connection = null;

 	public static Connection getConnection() {
        if (connection != null)
            return connection;
        else {
            try {
             Properties prop = new Properties();
             String driver,url,user,password;
                InputStream inputStream = DBUtility.class.getClassLoader().getResourceAsStream("config.properties");
                if(null!=inputStream){
                	System.out.println("Properties loading from properties file.. ");
                	prop.load(inputStream);
                	driver = prop.getProperty("driver");
                    url = prop.getProperty("url");
                    user = prop.getProperty("user");
                    password = prop.getProperty("password");
                }else{
                	System.out.println("Properties not loaded so getting from static class");
                	driver = DBDetails.driver;
                    url = DBDetails.url;
                    user = DBDetails.user;
                    password = DBDetails.password;
                }
                
                Class.forName(driver);
                connection = DriverManager.getConnection(url, user, password);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return connection;
        }

    }

}
