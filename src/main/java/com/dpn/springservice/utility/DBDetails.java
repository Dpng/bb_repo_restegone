package com.dpn.springservice.utility;

public final class DBDetails {
	
	public static final String driver = "com.mysql.jdbc.Driver";
	public static final String url = "jdbc:mysql://localhost:3306/testWS";
	public static final String user = "root";
	public static final String password = "sesame";

}
