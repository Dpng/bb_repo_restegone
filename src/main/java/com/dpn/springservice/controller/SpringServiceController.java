package com.dpn.springservice.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dpn.springservice.dao.UserService;
import com.dpn.springservice.domain.User;
/*
 * Spring RESTFul Web Service that would return user information in JSON format from an underlying MySql table
 */
@Controller
@RestController
@RequestMapping("/service/user/")
public class SpringServiceController {
 
 UserService userService = new UserService();
           
 @RequestMapping(value = "/{id}", method = RequestMethod.GET,headers="Accept=application/json")
 public User getUser(@PathVariable int id) {
	
 User user=userService.getUserById(id);
  return user;
 }
 
 @RequestMapping(method = RequestMethod.GET,headers="Accept=application/json")
 public List<User> getAllUsers() {
  List<User> users=userService.getAllUsers();
  return users;
 }
 
 
}
